use gl;
use gl::types::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Blending {
    None,
    Default,
    Additive,
    Subtractive,
    Multiply,
}
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum CullFace {
    None,
    Back,
    Front,
    FrontAndBack,
}
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Depth {
    None,
    Never,
    LessThan,
    Equal,
    LessThanOrEqual,
    GreaterThan,
    NotEqual,
    GreaterThanOrEqual,
    Always,
}
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum FilterMode {
    None,
    Linear,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum DrawMode {
    Points,
    LineStrip,
    LineLoop,
    Lines,
    LineStripAdjacency,
    LineAdjacency,
    TriangleStrip,
    TriangleFan,
    Triangles,
    TriangleStripAdjacency,
    TrianglesAdjacency,
    Patches,
}

impl From<GLenum> for DrawMode {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::POINTS => DrawMode::Points,
            gl::LINE_STRIP => DrawMode::LineStrip,
            gl::LINE_LOOP => DrawMode::LineLoop,
            gl::LINES => DrawMode::Lines,
            gl::LINE_STRIP_ADJACENCY => DrawMode::LineStripAdjacency,
            gl::LINES_ADJACENCY => DrawMode::LineAdjacency,
            gl::TRIANGLE_STRIP => DrawMode::TriangleStrip,
            gl::TRIANGLE_FAN => DrawMode::TriangleFan,
            gl::TRIANGLES => DrawMode::Triangles,
            gl::TRIANGLE_STRIP_ADJACENCY => DrawMode::TriangleStripAdjacency,
            gl::TRIANGLES_ADJACENCY => DrawMode::TrianglesAdjacency,
            gl::PATCHES => DrawMode::Patches,
            _ => panic!("Invalid GLenum {:?} for DrawMode", gl_enum),
        }
    }
}

impl<'a> From<&'a DrawMode> for GLenum {
    #[inline]
    fn from(draw_mode: &'a DrawMode) -> Self {
        match draw_mode {
            &DrawMode::Points => gl::POINTS,
            &DrawMode::LineStrip => gl::LINE_STRIP,
            &DrawMode::LineLoop => gl::LINE_LOOP,
            &DrawMode::Lines => gl::LINES,
            &DrawMode::LineStripAdjacency => gl::LINE_STRIP_ADJACENCY,
            &DrawMode::LineAdjacency => gl::LINES_ADJACENCY,
            &DrawMode::TriangleStrip => gl::TRIANGLE_STRIP,
            &DrawMode::TriangleFan => gl::TRIANGLE_FAN,
            &DrawMode::Triangles => gl::TRIANGLES,
            &DrawMode::TriangleStripAdjacency => gl::TRIANGLE_STRIP_ADJACENCY,
            &DrawMode::TrianglesAdjacency => gl::TRIANGLES_ADJACENCY,
            &DrawMode::Patches => gl::PATCHES,
        }
    }
}

impl From<DrawMode> for GLenum {
    #[inline(always)]
    fn from(draw_mode: DrawMode) -> Self {
        From::from(&draw_mode)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum IndexKind {
    UnsignedByte,
    UnsignedShort,
    UnsignedInt,
}

impl From<GLenum> for IndexKind {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::UNSIGNED_BYTE => IndexKind::UnsignedByte,
            gl::UNSIGNED_SHORT => IndexKind::UnsignedShort,
            gl::UNSIGNED_INT => IndexKind::UnsignedInt,
            _ => panic!("Invalid GLenum {:?} for IndexKind", gl_enum),
        }
    }
}
impl<'a> From<&'a IndexKind> for GLenum {
    #[inline]
    fn from(index_kind: &'a IndexKind) -> Self {
        match index_kind {
            &IndexKind::UnsignedByte => gl::UNSIGNED_BYTE,
            &IndexKind::UnsignedShort => gl::UNSIGNED_SHORT,
            &IndexKind::UnsignedInt => gl::UNSIGNED_INT,
        }
    }
}
impl From<IndexKind> for GLenum {
    #[inline(always)]
    fn from(index_kind: IndexKind) -> Self {
        From::from(&index_kind)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Error {
    NoError,
    InvalidEnum,
    InvalidValue,
    InvalidOperation,
    InvalidFramebufferOperation,
    StackUnderflow,
    StackOverflow,
    OutOfMemory,
}

impl From<GLenum> for Error {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::NO_ERROR => Error::NoError,
            gl::INVALID_ENUM => Error::InvalidEnum,
            gl::INVALID_VALUE => Error::InvalidValue,
            gl::INVALID_OPERATION => Error::InvalidOperation,
            gl::INVALID_FRAMEBUFFER_OPERATION => Error::InvalidFramebufferOperation,
            gl::STACK_UNDERFLOW => Error::StackUnderflow,
            gl::STACK_OVERFLOW => Error::StackOverflow,
            gl::OUT_OF_MEMORY => Error::OutOfMemory,
            _ => panic!("Invalid GLenum {:?} for Error", gl_enum),
        }
    }
}
impl<'a> From<&'a Error> for GLenum {
    #[inline]
    fn from(error: &'a Error) -> Self {
        match error {
            &Error::NoError => gl::NO_ERROR,
            &Error::InvalidEnum => gl::INVALID_ENUM,
            &Error::InvalidValue => gl::INVALID_VALUE,
            &Error::InvalidOperation => gl::INVALID_OPERATION,
            &Error::InvalidFramebufferOperation => gl::INVALID_FRAMEBUFFER_OPERATION,
            &Error::StackUnderflow => gl::STACK_UNDERFLOW,
            &Error::StackOverflow => gl::STACK_OVERFLOW,
            &Error::OutOfMemory => gl::OUT_OF_MEMORY,
        }
    }
}
impl From<Error> for GLenum {
    #[inline(always)]
    fn from(error: Error) -> Self {
        From::from(&error)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Usage {
    StreamDraw,
    StreamRead,
    StreamCopy,
    StaticDraw,
    StaticRead,
    StaticCopy,
    DynamicDraw,
    DynamicRead,
    DynamicCopy,
}

impl From<GLenum> for Usage {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::STREAM_DRAW => Usage::StreamDraw,
            gl::STREAM_READ => Usage::StreamRead,
            gl::STREAM_COPY => Usage::StreamCopy,

            gl::STATIC_DRAW => Usage::StaticDraw,
            gl::STATIC_READ => Usage::StaticRead,
            gl::STATIC_COPY => Usage::StaticCopy,

            gl::DYNAMIC_DRAW => Usage::DynamicDraw,
            gl::DYNAMIC_READ => Usage::DynamicRead,
            gl::DYNAMIC_COPY => Usage::DynamicCopy,

            _ => panic!("Invalid GLenum {:?} for Usage", gl_enum),
        }
    }
}
impl<'a> From<&'a Usage> for GLenum {
    #[inline]
    fn from(usage: &'a Usage) -> Self {
        match usage {
            &Usage::StreamDraw => gl::STREAM_DRAW,
            &Usage::StreamRead => gl::STREAM_READ,
            &Usage::StreamCopy => gl::STREAM_COPY,

            &Usage::StaticDraw => gl::STATIC_DRAW,
            &Usage::StaticRead => gl::STATIC_READ,
            &Usage::StaticCopy => gl::STATIC_COPY,

            &Usage::DynamicDraw => gl::DYNAMIC_DRAW,
            &Usage::DynamicRead => gl::DYNAMIC_READ,
            &Usage::DynamicCopy => gl::DYNAMIC_COPY,
        }
    }
}
impl From<Usage> for GLenum {
    #[inline(always)]
    fn from(usage: Usage) -> Self {
        From::from(&usage)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum BufferTarget {
    Array,
    AtomicCounter,
    CopyRead,
    CopyWrite,
    DispatchIndirect,
    DrawIndirect,
    ElementArray,
    PixelPack,
    PixelUnpack,
    Query,
    ShaderStorage,
    Texture,
    TransformFeedback,
    UniformKind,
}

impl From<GLenum> for BufferTarget {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::ARRAY_BUFFER => BufferTarget::Array,
            gl::ATOMIC_COUNTER_BUFFER => BufferTarget::AtomicCounter,
            gl::COPY_READ_BUFFER => BufferTarget::CopyRead,
            gl::COPY_WRITE_BUFFER => BufferTarget::CopyWrite,
            gl::DISPATCH_INDIRECT_BUFFER => BufferTarget::DispatchIndirect,
            gl::DRAW_INDIRECT_BUFFER => BufferTarget::DrawIndirect,
            gl::ELEMENT_ARRAY_BUFFER => BufferTarget::ElementArray,
            gl::PIXEL_PACK_BUFFER => BufferTarget::PixelPack,
            gl::PIXEL_UNPACK_BUFFER => BufferTarget::PixelUnpack,
            gl::QUERY_BUFFER => BufferTarget::Query,
            gl::SHADER_STORAGE_BUFFER => BufferTarget::ShaderStorage,
            gl::TEXTURE_BUFFER => BufferTarget::Texture,
            gl::TRANSFORM_FEEDBACK_BUFFER => BufferTarget::TransformFeedback,
            gl::UNIFORM_BUFFER => BufferTarget::UniformKind,
            _ => panic!("Invalid GLenum {:?} for BufferTarget", gl_enum),
        }
    }
}
impl<'a> From<&'a BufferTarget> for GLenum {
    #[inline]
    fn from(buffer_target: &'a BufferTarget) -> Self {
        match buffer_target {
            &BufferTarget::Array => gl::ARRAY_BUFFER,
            &BufferTarget::AtomicCounter => gl::ATOMIC_COUNTER_BUFFER,
            &BufferTarget::CopyRead => gl::COPY_READ_BUFFER,
            &BufferTarget::CopyWrite => gl::COPY_WRITE_BUFFER,
            &BufferTarget::DispatchIndirect => gl::DISPATCH_INDIRECT_BUFFER,
            &BufferTarget::DrawIndirect => gl::DRAW_INDIRECT_BUFFER,
            &BufferTarget::ElementArray => gl::ELEMENT_ARRAY_BUFFER,
            &BufferTarget::PixelPack => gl::PIXEL_PACK_BUFFER,
            &BufferTarget::PixelUnpack => gl::PIXEL_UNPACK_BUFFER,
            &BufferTarget::Query => gl::QUERY_BUFFER,
            &BufferTarget::ShaderStorage => gl::SHADER_STORAGE_BUFFER,
            &BufferTarget::Texture => gl::TEXTURE_BUFFER,
            &BufferTarget::TransformFeedback => gl::TRANSFORM_FEEDBACK_BUFFER,
            &BufferTarget::UniformKind => gl::UNIFORM_BUFFER,
        }
    }
}
impl From<BufferTarget> for GLenum {
    #[inline(always)]
    fn from(buffer_target: BufferTarget) -> Self {
        From::from(&buffer_target)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum TextureKind {
    Texture1D,
    Texture2D,
    Texture3D,
    Texture1DArray,
    Texture2DArray,
    TextureRectangle,
    TextureCubeMap,
    TextureCubeMapArray,
    TextureBuffer,
    Texture2DMultiSameple,
    Texture2DMultiSamepleArray,
}

impl From<GLenum> for TextureKind {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::TEXTURE_1D => TextureKind::Texture1D,
            gl::TEXTURE_2D => TextureKind::Texture2D,
            gl::TEXTURE_3D => TextureKind::Texture3D,
            gl::TEXTURE_1D_ARRAY => TextureKind::Texture1DArray,
            gl::TEXTURE_2D_ARRAY => TextureKind::Texture2DArray,
            gl::TEXTURE_RECTANGLE => TextureKind::TextureRectangle,
            gl::TEXTURE_CUBE_MAP => TextureKind::TextureCubeMap,
            gl::TEXTURE_CUBE_MAP_ARRAY => TextureKind::TextureCubeMapArray,
            gl::TEXTURE_BUFFER => TextureKind::TextureBuffer,
            gl::TEXTURE_2D_MULTISAMPLE => TextureKind::Texture2DMultiSameple,
            gl::TEXTURE_2D_MULTISAMPLE_ARRAY => TextureKind::Texture2DMultiSamepleArray,
            _ => panic!("Invalid GLenum {:?} for TextureKind", gl_enum),
        }
    }
}
impl<'a> From<&'a TextureKind> for GLenum {
    #[inline]
    fn from(texture_kind: &'a TextureKind) -> Self {
        match texture_kind {
            &TextureKind::Texture1D => gl::TEXTURE_1D,
            &TextureKind::Texture2D => gl::TEXTURE_2D,
            &TextureKind::Texture3D => gl::TEXTURE_3D,
            &TextureKind::Texture1DArray => gl::TEXTURE_1D_ARRAY,
            &TextureKind::Texture2DArray => gl::TEXTURE_2D_ARRAY,
            &TextureKind::TextureRectangle => gl::TEXTURE_RECTANGLE,
            &TextureKind::TextureCubeMap => gl::TEXTURE_CUBE_MAP,
            &TextureKind::TextureCubeMapArray => gl::TEXTURE_CUBE_MAP_ARRAY,
            &TextureKind::TextureBuffer => gl::TEXTURE_BUFFER,
            &TextureKind::Texture2DMultiSameple => gl::TEXTURE_2D_MULTISAMPLE,
            &TextureKind::Texture2DMultiSamepleArray => gl::TEXTURE_2D_MULTISAMPLE_ARRAY,
        }
    }
}
impl From<TextureKind> for GLenum {
    #[inline(always)]
    fn from(texture_kind: TextureKind) -> Self {
        From::from(&texture_kind)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Attachment {
    Color(usize),
    Depth,
    Stencil,
}

impl From<GLenum> for Attachment {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::DEPTH_ATTACHMENT => Attachment::Depth,
            gl::STENCIL_ATTACHMENT => Attachment::Stencil,
            _ => if gl_enum >= gl::COLOR_ATTACHMENT0 {
                Attachment::Color((gl_enum - gl::COLOR_ATTACHMENT0) as usize)
            } else {
                panic!("Invalid GLenum {:?} for Attachment", gl_enum)
            },
        }
    }
}
impl<'a> From<&'a Attachment> for GLenum {
    #[inline]
    fn from(attachment: &'a Attachment) -> Self {
        match attachment {
            &Attachment::Color(index) => gl::COLOR_ATTACHMENT0 + (index as GLenum),
            &Attachment::Depth => gl::DEPTH_ATTACHMENT,
            &Attachment::Stencil => gl::STENCIL_ATTACHMENT,
        }
    }
}
impl From<Attachment> for GLenum {
    #[inline(always)]
    fn from(attachment: Attachment) -> Self {
        From::from(&attachment)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum DataFormat {
    Red,
    RG,
    RGB,
    BGR,
    RGBA,
    BGRA,
    RedInteger,
    RGInteger,
    RGBInteger,
    BGRInteger,
    RGBAInteger,
    BGRAInteger,
    StencilInteger,
    DepthComponent,
    DepthStencil,
}

impl From<GLenum> for DataFormat {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::RED => DataFormat::Red,
            gl::RG => DataFormat::RG,
            gl::RGB => DataFormat::RGB,
            gl::BGR => DataFormat::BGR,
            gl::RGBA => DataFormat::RGBA,
            gl::BGRA => DataFormat::BGRA,
            gl::RED_INTEGER => DataFormat::RedInteger,
            gl::RG_INTEGER => DataFormat::RGInteger,
            gl::RGB_INTEGER => DataFormat::RGBInteger,
            gl::BGR_INTEGER => DataFormat::BGRInteger,
            gl::RGBA_INTEGER => DataFormat::RGBAInteger,
            gl::BGRA_INTEGER => DataFormat::BGRAInteger,
            gl::STENCIL_INDEX => DataFormat::StencilInteger,
            gl::DEPTH_COMPONENT => DataFormat::DepthComponent,
            gl::DEPTH_STENCIL => DataFormat::DepthStencil,
            _ => panic!("Invalid GLenum {:?} for DataFormat", gl_enum),
        }
    }
}
impl<'a> From<&'a DataFormat> for GLenum {
    #[inline]
    fn from(data_format: &'a DataFormat) -> Self {
        match data_format {
            &DataFormat::Red => gl::RED,
            &DataFormat::RG => gl::RG,
            &DataFormat::RGB => gl::RGB,
            &DataFormat::BGR => gl::BGR,
            &DataFormat::RGBA => gl::RGBA,
            &DataFormat::BGRA => gl::BGRA,
            &DataFormat::RedInteger => gl::RED_INTEGER,
            &DataFormat::RGInteger => gl::RG_INTEGER,
            &DataFormat::RGBInteger => gl::RGB_INTEGER,
            &DataFormat::BGRInteger => gl::BGR_INTEGER,
            &DataFormat::RGBAInteger => gl::RGBA_INTEGER,
            &DataFormat::BGRAInteger => gl::BGRA_INTEGER,
            &DataFormat::StencilInteger => gl::STENCIL_INDEX,
            &DataFormat::DepthComponent => gl::DEPTH_COMPONENT,
            &DataFormat::DepthStencil => gl::DEPTH_STENCIL,
        }
    }
}
impl From<DataFormat> for GLenum {
    #[inline(always)]
    fn from(data_format: DataFormat) -> Self {
        From::from(&data_format)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum DataKind {
    UnsignedByte,
    Byte,
    UnsignedShort,
    Short,
    UnsignedInt,
    Int,
    Float,
    Bool,

    UnsignedByte332,
    UnsignedByte223Rev,

    UnsignedShort565,
    UnsignedShort565Rev,

    UnsignedShort4444,
    UnsignedShort4444Rev,

    UnsignedShort5551,
    UnsignedShort1555Rev,

    UnsignedInt8888,
    UnsignedInt8888Rev,

    UnsignedInt1010102,
    UnsignedInt2101010Rev,
}

impl From<GLenum> for DataKind {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::UNSIGNED_BYTE => DataKind::UnsignedByte,
            gl::BYTE => DataKind::Byte,
            gl::UNSIGNED_SHORT => DataKind::UnsignedShort,
            gl::SHORT => DataKind::Short,
            gl::UNSIGNED_INT => DataKind::UnsignedInt,
            gl::INT => DataKind::Int,
            gl::FLOAT => DataKind::Float,
            gl::BOOL => DataKind::Bool,

            gl::UNSIGNED_BYTE_3_3_2 => DataKind::UnsignedByte332,
            gl::UNSIGNED_BYTE_2_3_3_REV => DataKind::UnsignedByte223Rev,

            gl::UNSIGNED_SHORT_5_6_5 => DataKind::UnsignedShort565,
            gl::UNSIGNED_SHORT_5_6_5_REV => DataKind::UnsignedShort565Rev,

            gl::UNSIGNED_SHORT_4_4_4_4 => DataKind::UnsignedShort4444,
            gl::UNSIGNED_SHORT_4_4_4_4_REV => DataKind::UnsignedShort4444Rev,

            gl::UNSIGNED_SHORT_5_5_5_1 => DataKind::UnsignedShort5551,
            gl::UNSIGNED_SHORT_1_5_5_5_REV => DataKind::UnsignedShort1555Rev,

            gl::UNSIGNED_INT_8_8_8_8 => DataKind::UnsignedInt8888,
            gl::UNSIGNED_INT_8_8_8_8_REV => DataKind::UnsignedInt8888Rev,

            gl::UNSIGNED_INT_10_10_10_2 => DataKind::UnsignedInt1010102,
            gl::UNSIGNED_INT_2_10_10_10_REV => DataKind::UnsignedInt2101010Rev,

            _ => panic!("Invalid GLenum {:?} for DataKind", gl_enum),
        }
    }
}
impl<'a> From<&'a DataKind> for GLenum {
    #[inline]
    fn from(data_kind: &'a DataKind) -> Self {
        match data_kind {
            &DataKind::UnsignedByte => gl::UNSIGNED_BYTE,
            &DataKind::Byte => gl::BYTE,
            &DataKind::UnsignedShort => gl::UNSIGNED_SHORT,
            &DataKind::Short => gl::SHORT,
            &DataKind::UnsignedInt => gl::UNSIGNED_INT,
            &DataKind::Int => gl::INT,
            &DataKind::Float => gl::FLOAT,
            &DataKind::Bool => gl::BOOL,

            &DataKind::UnsignedByte332 => gl::UNSIGNED_BYTE_3_3_2,
            &DataKind::UnsignedByte223Rev => gl::UNSIGNED_BYTE_2_3_3_REV,

            &DataKind::UnsignedShort565 => gl::UNSIGNED_SHORT_5_6_5,
            &DataKind::UnsignedShort565Rev => gl::UNSIGNED_SHORT_5_6_5_REV,

            &DataKind::UnsignedShort4444 => gl::UNSIGNED_SHORT_4_4_4_4,
            &DataKind::UnsignedShort4444Rev => gl::UNSIGNED_SHORT_4_4_4_4_REV,

            &DataKind::UnsignedShort5551 => gl::UNSIGNED_SHORT_5_5_5_1,
            &DataKind::UnsignedShort1555Rev => gl::UNSIGNED_SHORT_1_5_5_5_REV,

            &DataKind::UnsignedInt8888 => gl::UNSIGNED_INT_8_8_8_8,
            &DataKind::UnsignedInt8888Rev => gl::UNSIGNED_INT_8_8_8_8_REV,

            &DataKind::UnsignedInt1010102 => gl::UNSIGNED_INT_10_10_10_2,
            &DataKind::UnsignedInt2101010Rev => gl::UNSIGNED_INT_2_10_10_10_REV,
        }
    }
}
impl From<DataKind> for GLenum {
    #[inline(always)]
    fn from(data_kind: DataKind) -> Self {
        From::from(&data_kind)
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum InternalFormat {
    R8,
    R8_SNORM,
    R16F,
    R32F,
    R8UI,
    R8I,
    R16UI,
    R16I,
    R32UI,
    R32I,
    RG8,
    RG8_SNORM,
    RG16F,
    RG32F,
    RG8UI,
    RG8I,
    RG16UI,
    RG16I,
    RG32UI,
    RG32I,
    RGB8,
    SRGB8,
    RGB565,
    RGB8_SNORM,
    R11F_G11F_B10F,
    RGB9_E5,
    RGB16F,
    RGB32F,
    RGB8UI,
    RGB8I,
    RGB16UI,
    RGB16I,
    RGB32UI,
    RGB32I,
    RGBA,
    RGBA8,
    SRGB8_ALPHA8,
    RGBA8_SNORM,
    RGB5_A1,
    RGBA4,
    RGB10_A2,
    RGBA16F,
    RGBA32F,
    RGBA8UI,
    RGBA8I,
    RGB10_A2UI,
    RGBA16UI,
    RGBA16I,
    RGBA32I,
    RGBA32UI,
    DepthComponent16,
    DepthComponent24,
    DepthComponent32F,
    Depth24Stencil8,
    Depth32FStencil8,
    StencilIndex8,
}

impl From<GLenum> for InternalFormat {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::R8 => InternalFormat::R8,
            gl::R8_SNORM => InternalFormat::R8_SNORM,
            gl::R16F => InternalFormat::R16F,
            gl::R32F => InternalFormat::R32F,
            gl::R8UI => InternalFormat::R8UI,
            gl::R8I => InternalFormat::R8I,
            gl::R16UI => InternalFormat::R16UI,
            gl::R16I => InternalFormat::R16I,
            gl::R32UI => InternalFormat::R32UI,
            gl::R32I => InternalFormat::R32I,
            gl::RG8 => InternalFormat::RG8,
            gl::RG8_SNORM => InternalFormat::RG8_SNORM,
            gl::RG16F => InternalFormat::RG16F,
            gl::RG32F => InternalFormat::RG32F,
            gl::RG8UI => InternalFormat::RG8UI,
            gl::RG8I => InternalFormat::RG8I,
            gl::RG16UI => InternalFormat::RG16UI,
            gl::RG16I => InternalFormat::RG16I,
            gl::RG32UI => InternalFormat::RG32UI,
            gl::RG32I => InternalFormat::RG32I,
            gl::RGB8 => InternalFormat::RGB8,
            gl::SRGB8 => InternalFormat::SRGB8,
            gl::RGB565 => InternalFormat::RGB565,
            gl::RGB8_SNORM => InternalFormat::RGB8_SNORM,
            gl::R11F_G11F_B10F => InternalFormat::R11F_G11F_B10F,
            gl::RGB9_E5 => InternalFormat::RGB9_E5,
            gl::RGB16F => InternalFormat::RGB16F,
            gl::RGB32F => InternalFormat::RGB32F,
            gl::RGB8UI => InternalFormat::RGB8UI,
            gl::RGB8I => InternalFormat::RGB8I,
            gl::RGB16UI => InternalFormat::RGB16UI,
            gl::RGB16I => InternalFormat::RGB16I,
            gl::RGB32UI => InternalFormat::RGB32UI,
            gl::RGB32I => InternalFormat::RGB32I,
            gl::RGBA => InternalFormat::RGBA,
            gl::RGBA8 => InternalFormat::RGBA8,
            gl::SRGB8_ALPHA8 => InternalFormat::SRGB8_ALPHA8,
            gl::RGBA8_SNORM => InternalFormat::RGBA8_SNORM,
            gl::RGB5_A1 => InternalFormat::RGB5_A1,
            gl::RGBA4 => InternalFormat::RGBA4,
            gl::RGB10_A2 => InternalFormat::RGB10_A2,
            gl::RGBA16F => InternalFormat::RGBA16F,
            gl::RGBA32F => InternalFormat::RGBA32F,
            gl::RGBA8UI => InternalFormat::RGBA8UI,
            gl::RGBA8I => InternalFormat::RGBA8I,
            gl::RGB10_A2UI => InternalFormat::RGB10_A2UI,
            gl::RGBA16UI => InternalFormat::RGBA16UI,
            gl::RGBA16I => InternalFormat::RGBA16I,
            gl::RGBA32I => InternalFormat::RGBA32I,
            gl::RGBA32UI => InternalFormat::RGBA32UI,
            gl::DEPTH_COMPONENT16 => InternalFormat::DepthComponent16,
            gl::DEPTH_COMPONENT24 => InternalFormat::DepthComponent24,
            gl::DEPTH_COMPONENT32F => InternalFormat::DepthComponent32F,
            gl::DEPTH24_STENCIL8 => InternalFormat::Depth24Stencil8,
            gl::DEPTH32F_STENCIL8 => InternalFormat::Depth32FStencil8,
            gl::STENCIL_INDEX8 => InternalFormat::StencilIndex8,
            _ => panic!("Invalid GLenum {:?} for InternalFormat", gl_enum),
        }
    }
}
impl<'a> From<&'a InternalFormat> for GLenum {
    #[inline]
    fn from(internal_format: &'a InternalFormat) -> Self {
        match internal_format {
            &InternalFormat::R8 => gl::R8,
            &InternalFormat::R8_SNORM => gl::R8_SNORM,
            &InternalFormat::R16F => gl::R16F,
            &InternalFormat::R32F => gl::R32F,
            &InternalFormat::R8UI => gl::R8UI,
            &InternalFormat::R8I => gl::R8I,
            &InternalFormat::R16UI => gl::R16UI,
            &InternalFormat::R16I => gl::R16I,
            &InternalFormat::R32UI => gl::R32UI,
            &InternalFormat::R32I => gl::R32I,
            &InternalFormat::RG8 => gl::RG8,
            &InternalFormat::RG8_SNORM => gl::RG8_SNORM,
            &InternalFormat::RG16F => gl::RG16F,
            &InternalFormat::RG32F => gl::RG32F,
            &InternalFormat::RG8UI => gl::RG8UI,
            &InternalFormat::RG8I => gl::RG8I,
            &InternalFormat::RG16UI => gl::RG16UI,
            &InternalFormat::RG16I => gl::RG16I,
            &InternalFormat::RG32UI => gl::RG32UI,
            &InternalFormat::RG32I => gl::RG32I,
            &InternalFormat::RGB8 => gl::RGB8,
            &InternalFormat::SRGB8 => gl::SRGB8,
            &InternalFormat::RGB565 => gl::RGB565,
            &InternalFormat::RGB8_SNORM => gl::RGB8_SNORM,
            &InternalFormat::R11F_G11F_B10F => gl::R11F_G11F_B10F,
            &InternalFormat::RGB9_E5 => gl::RGB9_E5,
            &InternalFormat::RGB16F => gl::RGB16F,
            &InternalFormat::RGB32F => gl::RGB32F,
            &InternalFormat::RGB8UI => gl::RGB8UI,
            &InternalFormat::RGB8I => gl::RGB8I,
            &InternalFormat::RGB16UI => gl::RGB16UI,
            &InternalFormat::RGB16I => gl::RGB16I,
            &InternalFormat::RGB32UI => gl::RGB32UI,
            &InternalFormat::RGB32I => gl::RGB32I,
            &InternalFormat::RGBA => gl::RGBA,
            &InternalFormat::RGBA8 => gl::RGBA8,
            &InternalFormat::SRGB8_ALPHA8 => gl::SRGB8_ALPHA8,
            &InternalFormat::RGBA8_SNORM => gl::RGBA8_SNORM,
            &InternalFormat::RGB5_A1 => gl::RGB5_A1,
            &InternalFormat::RGBA4 => gl::RGBA4,
            &InternalFormat::RGB10_A2 => gl::RGB10_A2,
            &InternalFormat::RGBA16F => gl::RGBA16F,
            &InternalFormat::RGBA32F => gl::RGBA32F,
            &InternalFormat::RGBA8UI => gl::RGBA8UI,
            &InternalFormat::RGBA8I => gl::RGBA8I,
            &InternalFormat::RGB10_A2UI => gl::RGB10_A2UI,
            &InternalFormat::RGBA16UI => gl::RGBA16UI,
            &InternalFormat::RGBA16I => gl::RGBA16I,
            &InternalFormat::RGBA32I => gl::RGBA32I,
            &InternalFormat::RGBA32UI => gl::RGBA32UI,
            &InternalFormat::DepthComponent16 => gl::DEPTH_COMPONENT16,
            &InternalFormat::DepthComponent24 => gl::DEPTH_COMPONENT24,
            &InternalFormat::DepthComponent32F => gl::DEPTH_COMPONENT32F,
            &InternalFormat::Depth24Stencil8 => gl::DEPTH24_STENCIL8,
            &InternalFormat::Depth32FStencil8 => gl::DEPTH32F_STENCIL8,
            &InternalFormat::StencilIndex8 => gl::STENCIL_INDEX8,
        }
    }
}
impl From<InternalFormat> for GLenum {
    #[inline(always)]
    fn from(internal_format: InternalFormat) -> Self {
        From::from(&internal_format)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum AttributeKind {
    Bool,
    Int,
    Float,
    BoolVec2,
    IntVec2,
    FloatVec2,
    BoolVec3,
    IntVec3,
    FloatVec3,
    BoolVec4,
    IntVec4,
    FloatVec4,
}

impl From<GLenum> for AttributeKind {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::BOOL => AttributeKind::Bool,
            gl::INT => AttributeKind::Int,
            gl::FLOAT => AttributeKind::Float,

            gl::BOOL_VEC2 => AttributeKind::BoolVec2,
            gl::INT_VEC2 => AttributeKind::IntVec2,
            gl::FLOAT_VEC2 => AttributeKind::FloatVec2,

            gl::BOOL_VEC3 => AttributeKind::BoolVec3,
            gl::INT_VEC3 => AttributeKind::IntVec3,
            gl::FLOAT_VEC3 => AttributeKind::FloatVec3,

            gl::BOOL_VEC4 => AttributeKind::BoolVec4,
            gl::INT_VEC4 => AttributeKind::IntVec4,
            gl::FLOAT_VEC4 => AttributeKind::FloatVec4,

            _ => panic!("Invalid GLenum {:?} for AttributeKind", gl_enum),
        }
    }
}

impl<'a> From<&'a AttributeKind> for GLenum {
    #[inline]
    fn from(attribute_kind: &'a AttributeKind) -> Self {
        match attribute_kind {
            &AttributeKind::Bool => gl::BOOL,
            &AttributeKind::Int => gl::INT,
            &AttributeKind::Float => gl::FLOAT,

            &AttributeKind::BoolVec2 => gl::BOOL_VEC2,
            &AttributeKind::IntVec2 => gl::INT_VEC2,
            &AttributeKind::FloatVec2 => gl::FLOAT_VEC2,

            &AttributeKind::BoolVec3 => gl::BOOL_VEC3,
            &AttributeKind::IntVec3 => gl::INT_VEC3,
            &AttributeKind::FloatVec3 => gl::FLOAT_VEC3,

            &AttributeKind::BoolVec4 => gl::BOOL_VEC4,
            &AttributeKind::IntVec4 => gl::INT_VEC4,
            &AttributeKind::FloatVec4 => gl::FLOAT_VEC4,
        }
    }
}
impl From<AttributeKind> for GLenum {
    #[inline(always)]
    fn from(attribute_kind: AttributeKind) -> Self {
        From::from(&attribute_kind)
    }
}

impl AttributeKind {
    #[inline]
    pub fn item_data(&self) -> (usize, DataKind) {
        match self {
            &AttributeKind::Bool => (1, DataKind::Bool),
            &AttributeKind::Int => (1, DataKind::Int),
            &AttributeKind::Float => (1, DataKind::Float),

            &AttributeKind::BoolVec2 => (2, DataKind::Bool),
            &AttributeKind::IntVec2 => (2, DataKind::Int),
            &AttributeKind::FloatVec2 => (2, DataKind::Float),

            &AttributeKind::BoolVec3 => (3, DataKind::Bool),
            &AttributeKind::IntVec3 => (3, DataKind::Int),
            &AttributeKind::FloatVec3 => (3, DataKind::Float),

            &AttributeKind::BoolVec4 => (4, DataKind::Bool),
            &AttributeKind::IntVec4 => (4, DataKind::Int),
            &AttributeKind::FloatVec4 => (4, DataKind::Float),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum UniformKind {
    Float,
    Int,

    FloatVec2,
    IntVec2,

    FloatVec3,
    IntVec3,

    FloatVec4,
    IntVec4,

    FloatMat2,
    FloatMat3,
    FloatMat4,

    Sampler2D,
}

impl From<GLenum> for UniformKind {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::FLOAT => UniformKind::Float,
            gl::INT => UniformKind::Int,

            gl::FLOAT_VEC2 => UniformKind::FloatVec2,
            gl::INT_VEC2 => UniformKind::IntVec2,

            gl::FLOAT_VEC3 => UniformKind::FloatVec3,
            gl::INT_VEC3 => UniformKind::IntVec3,

            gl::FLOAT_VEC4 => UniformKind::FloatVec4,
            gl::INT_VEC4 => UniformKind::IntVec4,

            gl::FLOAT_MAT2 => UniformKind::FloatMat2,
            gl::FLOAT_MAT3 => UniformKind::FloatMat3,
            gl::FLOAT_MAT4 => UniformKind::FloatMat4,

            gl::SAMPLER_2D => UniformKind::Sampler2D,

            _ => panic!("Invalid GLenum {:?} for UniformKind", gl_enum),
        }
    }
}

impl<'a> From<&'a UniformKind> for GLenum {
    #[inline]
    fn from(uniform_kind: &'a UniformKind) -> Self {
        match uniform_kind {
            &UniformKind::Float => gl::FLOAT,
            &UniformKind::Int => gl::INT,

            &UniformKind::FloatVec2 => gl::FLOAT_VEC2,
            &UniformKind::IntVec2 => gl::INT_VEC2,

            &UniformKind::FloatVec3 => gl::FLOAT_VEC3,
            &UniformKind::IntVec3 => gl::INT_VEC3,

            &UniformKind::FloatVec4 => gl::FLOAT_VEC4,
            &UniformKind::IntVec4 => gl::INT_VEC4,

            &UniformKind::FloatMat2 => gl::FLOAT_MAT2,
            &UniformKind::FloatMat3 => gl::FLOAT_MAT3,
            &UniformKind::FloatMat4 => gl::FLOAT_MAT4,

            &UniformKind::Sampler2D => gl::SAMPLER_2D,
        }
    }
}
impl From<UniformKind> for GLenum {
    #[inline(always)]
    fn from(uniform_kind: UniformKind) -> Self {
        From::from(&uniform_kind)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Wrap {
    Repeat,
    Clamp,
    MirroredRepeat,
}

impl From<GLenum> for Wrap {
    #[inline]
    fn from(gl_enum: GLenum) -> Self {
        match gl_enum {
            gl::REPEAT => Wrap::Repeat,
            gl::CLAMP_TO_EDGE => Wrap::Clamp,
            gl::MIRRORED_REPEAT => Wrap::MirroredRepeat,
            _ => panic!("Invalid GLenum {:?} for Wrap", gl_enum),
        }
    }
}

impl<'a> From<&'a Wrap> for GLenum {
    #[inline]
    fn from(wrap: &'a Wrap) -> Self {
        match wrap {
            &Wrap::Repeat => gl::REPEAT,
            &Wrap::Clamp => gl::CLAMP_TO_EDGE,
            &Wrap::MirroredRepeat => gl::MIRRORED_REPEAT,
        }
    }
}
impl From<Wrap> for GLenum {
    #[inline(always)]
    fn from(wrap: Wrap) -> Self {
        From::from(&wrap)
    }
}
